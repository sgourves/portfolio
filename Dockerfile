FROM alpine:3.19.3

LABEL Maintainer="PapierPain <papierpain4287@outlook.fr>"
LABEL Description="Portfolio Steven Gourves"

WORKDIR /portfolio

RUN apk add --update --no-cache bash nginx \
  php81 php81-fpm php81-opcache \
  php81-gd php81-zlib php81-curl \
  php81-mbstring php81-ldap php81-openssl \
  php81-session php81-tokenizer

COPY conf/nginx.conf /etc/nginx/http.d/default.conf
COPY app/ ./

EXPOSE 80

CMD ["/bin/bash", "-c", "php-fpm81 && nginx -g 'daemon off;'"]
