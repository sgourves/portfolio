<?php

// CONFIG APP
setlocale(LC_ALL, 'fr_FR.UTF8');

// VARIABLES
define('SITE_NAME', "Portfolio");
define('SITE_MAIL', "gourves.seven@gmail.com");


// MODELS
require 'models/routers/Route.php';
require 'models/routers/Router.php';
require 'models/routers/RouterException.php';
require 'models/File.php';
require 'models/Mail.php';

// VENDOR
require 'vendor/PHPMailer/src/Exception.php';
require 'vendor/PHPMailer/src/PHPMailer.php';
require 'vendor/PHPMailer/src/SMTP.php';

