<?php

namespace Models;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


/**
 * Classe pour l'envoie de mail.
 *
 * @package Models
 */
class Mail
{

    /**
     * Envoie du mail.
     *
     * @return void
     */
    public static function send($firstname, $lastname, $email, $subject, $message, $check)
    {

        // *** TREATMENTS *** //
        $firstname = str_replace(
            array("\r", "\n"), 
            array(" ", " "), 
            strip_tags(trim($firstname))
        );
        $lastname = str_replace(
            array("\r", "\n"), 
            array(" ", " "), 
            strip_tags(trim($lastname))
        );
        $email = filter_var(
            trim($email), 
            FILTER_SANITIZE_EMAIL
        );
        $subject = trim($subject);
        $message = htmlspecialchars(trim($message));
        $message = wordwrap($message, 70, "\r\n");


        // *** CHECK FIELDS *** //
        if (empty($firstname) or empty($lastname) or empty($subject) or empty($message) or !$check or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            http_response_code(400);
            echo "Oups! Formulaire temporairement indisponible, veuillez réessayer ulterieurement.";
            exit;
        }


        // *** SEND *** //
        Mail::mailerSend($email, $firstname, $lastname, $subject, $message);
    }

    protected static function mailerSend($email, $firstname, $lastname, $subject, $message)
    {
        $mail = new PHPMailer(true);

        try {
            $mail->CharSet = PHPMailer::CHARSET_UTF8;

            $mail->SMTPDebug = false;
            $mail->isSMTP();

            $mail->Host = 'smtp-mail.outlook.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'papiernas@outlook.fr';
            $mail->Password = 'DAw2Xa4JnQrj5eK9xu8h5V7LgNNe2n9n';
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; //Enable implicit TLS encryption
            $mail->Port = 587; //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

            //Recipients
            $mail->setFrom('papiernas@outlook.fr', $firstname. ' ' .$lastname. ' <' .$email. '>');
            $mail->addAddress(SITE_MAIL);

            //Content
            $mail->isHTML(true); //Set email format to HTML
            $mail->Subject = SITE_NAME .' '. $subject;
            $mail->Body    = Mail::message($subject, $email, $firstname, $lastname, $message);
            $mail->AltBody = $message;

            $mail->send();
            http_response_code(200);
            echo 'Merci ! Votre mail a bien été envoyé.';
        } catch (Exception $e) {
            http_response_code(500);
            echo 'Oups! Formulaire temporairement indisponible, veuillez réessayer ulterieurement.';
        }
    }

    /**
     * Mise en forme du message en html.
     *
     * @param string $message
     *
     * @return false|string html
     */
    protected static function message($subject, $email, $firstname, $lastname, $message)
    {
        $date = date('d.m.Y');

        $content = file_get_contents(File::vendor('MailPortfolio/index.html'));

        $content = str_replace('%subject%', $subject, $content);
        $content = str_replace('%date%', $date, $content);
        $content = str_replace('%email%', $email, $content);
        $content = str_replace('%firstname%', $firstname, $content);
        $content = str_replace('%lastname%', $lastname, $content);
        $content = str_replace('%message%', $message, $content);
        
        return $content;
    }
}
