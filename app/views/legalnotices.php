<?php
use \Models\File;

$title = 'Legal Notices of my portfolio';
$description = 'Legal Notice - Website Terms & Conditions.';
$mainAttr = 'class="page";id="legal-notices"';
?>


<?php ob_start(); ?>
<h1>Legal Notices</h1>

<section class="text">
    <h2>I - Identification</h2>

    <h3>Site Owner</h3>

    <ul>
        <li>Name : Gourves Steven</li>
        <li>Address : 22300 Lannion, France</li>
        <li>Phone : <a href="tel:+33651070929">06 51 07 09 29</a></li>
        <li>E-mail : <a href="/contact-me">by this form</a></li>
        <li>Design : Gourves Steven</li>
    </ul>

    <h3>Hosting</h3>

    <ul>
        <li>Name : OVH</li>
        <li>Head office : 2 rue Kellermann - 59100 Roubaix - France</li>
        <li>Web site : <a href="https://www.ovh.com" target="_blank" rel="noopener noreferrer">www.ovh.com</a></li>
    </ul>

    <h3>Activities</h3>

    <p>Showcase site presenting my achievements and skills.</p>

    <h2>II - Notice on the use of cookies & personal data</h2>

    <h3>Definition</h3>

    <p>A cookie is a small computer file, a tracer. It is used to analyse the behaviour of users when they visit a website, read an e-mail, install or use a software or a mobile application.</p>

    <p>The period of validity of the consent is a maximum of 13 months.</p>

    <h3>Uses</h3>

    <p>This site does not use cookies.</p>

    <h3>Contact information (form)</h3>

    <p>By submitting the contact form, you agree that the information entered may be used to reply.</p>

    <p>For any complaint, opposition, or rectification of data, please contact me <a href="/contact-me">by this form</a>.</p>
</section>
<?php $content = ob_get_clean(); ?>


<?php
require(File::page('layout'));
?>
