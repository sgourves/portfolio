<?php
use \Models\File;

$title = 'Papierpain homemade servers for my projects';
$description = 'Papierpain homemade servers for many projects. Implementation of continous integration and network servers like gitea, seafile or documentation website.';

$pjtName = "PapierPain";
$pjtDate = "2020/2021";
$pjtType = "Network - Documentation - Servers";

$pjtDescription = 'Creation of multi-projects on a raspberry pi 4 to make a local server (<a href="https://www.papierpain.fr" target="_blank" rel="noopener noreferrer">website link</a>) :</br>- Continuous integration with droneCI for git projects (self-hosted) ;</br>- Documentation website (<a href="https://docs.papierpain.fr" target="_blank" rel="noopener noreferrer">available here</a>]) for all what I implement (mainly on my local network).';

$pjtImage[0] = "papierpain_01";
$pjtImage[1] = "papierpain_02";
$pjtImage[2] = "papierpain_03";
$pjtTechno[0] = "Raspberry pi 4";
$pjtTechno[1] = "DroneCI";
$pjtTechno[2] = "MKdocs (Python library)";
$previousPjt = "/newscript-2021";
$nextPjt = "/sspo-visual-design-2020";

require(File::page('projects/layout'));
?>
 