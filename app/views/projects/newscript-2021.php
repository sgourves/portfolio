<?php
use \Models\File;

$title = 'Newscript Website to manage a news contest';
$description = 'Application web de gestion de concours de nouvelle (PHP MVC, PostgreSQL). Projet réalisé dans le cadre académique lors de la deuxième année d\'ingénieur.';

$pjtName = "NewScript";
$pjtDate = "2021";
$pjtType = "Academic Project - Website - Database";

$pjtDescription = 'For the Web and Database courses (engineering degree), I had to realize in group of 4 a web application to manage a news contest. The first half of the project concerned the database implementation (UML diagram and PostgreSQL for the database management system). The second half was about the website development in PHP with an MVC model (without Framework).';

$pjtImage[0] = "newscript_01";
$pjtImage[1] = "newscript_02";
$pjtImage[2] = "newscript_03";
$pjtTechno[0] = "PHP";
$pjtTechno[1] = "PostgreSQL";
$pjtTechno[2] = "Git";
$previousPjt = "/sspo-visual-design-2020";
$nextPjt = "/papierpain-home-made-servers-2021";

require(File::page('projects/layout'));
?>
