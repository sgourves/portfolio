<?php
use \Models\File;

$body = 'project';

$imgSource = "projects/";
?>


<?php ob_start(); ?>
<div class="left">
    <h1><?= $pjtName ?><sub><?= $pjtDate ?></sub></h1>

    <p class="type"><?= $pjtType ?></p>

    <figure class="mobile">
        <picture>
            <source type="image/webp" srcset="<?= File::image($imgSource . $pjtImage[0] . ".webp") ?>">
            <source type="image/jpeg" srcset="<?= File::image($imgSource . $pjtImage[0] . ".jpg") ?>">
            <img width="570" height="216" src="<?= File::image($imgSource . $pjtImage[0] . ".jpg") ?>" alt="image 01">
        </picture>
    </figure>

    <p><?= $pjtDescription ?></p>
</div>

<div class="right">
    <div class="gallery">
        <figure>
            <picture>
                <source type="image/webp" srcset="<?= File::image($imgSource . $pjtImage[0] . ".webp") ?>">
                <source type="image/jpeg" srcset="<?= File::image($imgSource . $pjtImage[0] . ".jpg") ?>">
                <img width="570" height="216" src="<?= File::image($imgSource . $pjtImage[0] . ".jpg") ?>" alt="image 01">
            </picture>
        </figure>

        <figure>
            <picture>
                <source type="image/webp" srcset="<?= File::image($imgSource . $pjtImage[1] . ".webp") ?>">
                <source type="image/jpeg" srcset="<?= File::image($imgSource . $pjtImage[1] . ".jpg") ?>">
                <img width="275" height="216" src="<?= File::image($imgSource . $pjtImage[1] . ".jpg") ?>" alt="image 02">
            </picture>
        </figure>

        <figure>
            <picture>
                <source type="image/webp" srcset="<?= File::image($imgSource . $pjtImage[2] . ".webp") ?>">
                <source type="image/jpeg" srcset="<?= File::image($imgSource . $pjtImage[2] . ".jpg") ?>">
                <img width="275" height="216" src="<?= File::image($imgSource . $pjtImage[2] . ".jpg") ?>" alt="image 03">
            </picture>
        </figure>
    </div>

    <div class="technologies">
        <h2>Technologies</h2>
        <ul>
            <li><?= $pjtTechno[0] ?></li>
            <li><?= $pjtTechno[1] ?></li>
            <li><?= $pjtTechno[2] ?></li>
        </ul>
    </div>
</div>
<?php $content = ob_get_clean(); ?>


<?php ob_start(); ?>
<a href="<?= $previousPjt ?>">< Previous project</a>

<div>
    <p>© Copyright 2021 Steven Gourves</p>
    <a href='/my-boring-legal-notices' target='_blank'>Legal notices</a>
</div>

<a href="<?= $nextPjt ?>">Next project ></a>
<?php $footer = ob_get_clean(); ?>


<?php
require(File::page('layout'));
?>
 