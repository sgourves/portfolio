<?php
use \Models\File;

$pjtName = "Startup Week-end";
$pjtDate = "2019";
$pjtType = "Entrepreneurship - Visual identity";

$pjtDescription = 'Creation of a startup in only 54 hours (in groups of 4) at Vichy (France). Menu-mobile (the startup) proposes to companies to save time by reserving the menu of their employees in advance for the lunch. Realization of a market study (with Business Model Canvas) and a prototype of the mobile application.';

$pjtImage[0] = "gallery01";
$pjtImage[1] = "gallery02";
$pjtImage[2] = "gallery03";
$pjtTechno[0] = "Adobe XD";
$previousPjt = "/papierpain-home-made-servers-2021";
$nextPjt = "/newscript-2021";

require(File::page('projects/layout'));
?>
