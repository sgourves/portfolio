<?php
use \Models\File;

$title = 'SSPO Association of first aiders at sea';
$description = 'Creation of the website and the graphic charter of the association SSPO (Sauveteurs Secouristes du Pays des Olonnes).';

$pjtName = "SSPO";
$pjtDate = "2020";
$pjtType = "Website - Visual Identity";

$pjtDescription = 'Creation of the website of the association of first aiders at sea SSPO (Sauveteurs Secouristes du Pays des Olonnes). Realization of the graphic charter and the logo of the association (<a href="https://www.sspo.fr" target="_blank" rel="noopener noreferrer">website available here</a>).';

$pjtImage[0] = "sspo_01";
$pjtImage[1] = "sspo_02";
$pjtImage[2] = "sspo_03";
$pjtTechno[0] = "PHP (Wordpress)";
$pjtTechno[1] = "Illustrator";
$pjtTechno[2] = "InDesign";
$previousPjt = "/papierpain-home-made-servers-2021";
$nextPjt = "/newscript-2021";

require(File::page('projects/layout'));
?>