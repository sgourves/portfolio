<?php
use \Models\File;

$title = 'Error 500';
$description = '';
$mainAttr = 'class="page";id="legal-notices"';
?>


<?php ob_start(); ?>
<h1>500</h1>

<section class="text">
    <h2>Internal server error</h2>

    <p><?= isset($message) ? $message : '' ?></p>
</section>
<?php $content = ob_get_clean(); ?>


<?php
require(File::page('layout'));
?>
