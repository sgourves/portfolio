<?php
use \Models\File;

$title = 'Error 404';
$description = '';
$mainAttr = 'class="page";id="legal-notices"';
?>


<?php ob_start(); ?>
<h1>404</h1>

<section class="text">
    <h2>Not found error</h2>

    <p><?= isset($message) ? $message : '' ?></p>
</section>
<?php $content = ob_get_clean(); ?>


<?php
require(File::page('layout'));
?>
