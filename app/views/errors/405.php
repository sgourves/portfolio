<?php
use \Models\File;

$title = 'Error 405';
$description = '';
$mainAttr = 'class="page";id="legal-notices"';
?>


<?php ob_start(); ?>
<h1>405</h1>

<section class="text">
    <h2>Request method not found error</h2>

    <p><?= isset($message) ? $message : '' ?></p>
</section>
<?php $content = ob_get_clean(); ?>


<?php
require(File::page('layout'));
?>
