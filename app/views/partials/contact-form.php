<form id="ajax-contact" method="post">
    <div>
        <div class="form-part">
            <label class="sr-only" for="form-firstname">Firstname</label>
            <input type="text" class="form-control" id="form-firstname" name="firstname" placeholder="Firstname" minlength="3" required>
        </div>
    
        <div class="form-part">
            <label class="sr-only" for="form-lastname">Name</label>
            <input type="text" class="form-control" id="form-lastname" name="lastname" placeholder="Name" minlength="3" required>
        </div>
    </div>

    <div class="form-part">
        <label class="sr-only" for="form-email">E-Mail</label>
        <input class="form-control" id="form-email" type="email" name="email" placeholder="E-Mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
    </div class="form-part">

    <div class="form-part">
        <label class="sr-only" for="form-subject">Subject</label>
        <input type="text" class="form-control" id="form-subject" name="subject" placeholder="Subject" minlength="3" required>
    </div>
    
    <div class="form-part">
        <label class="sr-only" for="form-text">Message</label>
        <textarea class="form-control" id="form-text" name="message" minlength="10" placeholder="Message" rows="5" required></textarea>
    </div>
    
    <div class="form-part" id="confirmData">
        <input class="check-form-input sr-only" id="form-check" type="checkbox" name="checkContact" required>
        <label for="form-check" class="check-form-label">
            <svg width="18px" height="18px" viewBox="0 0 18 18">
                <path d="M1,9 L1,3.5 C1,2 2,1 3.5,1 L14.5,1 C16,1 17,2 17,3.5 L17,14.5 C17,16 16,17 14.5,17 L3.5,17 C2,17 1,16 1,14.5 L1,9 Z"></path>
                <polyline points="1 9 7 14 15 4"></polyline>
            </svg>
            <p class="stext">By submitting this form, you agree that the information entered may be used in the context of the contact request.</p>
        </label>
    </div>

    <input id="field-contact" type="text" name="contact">
        
    <div class="form-part" id="form-part-send"></div>
</form>
