<?php
use \Models\File;

$title = 'My Amazing School Career';
$description = 'I\'m Steven Gourves, a passionate software engineer. Come and discover the way I followed to become a software engineer.';
$mainAttr = 'class="page" id="career"';
?>


<?php ob_start(); ?>
<h1>My amazing career</h1>

<section class="text career">
    <ul>
        <li>
            <figure>
                <picture>
                    <source type="image/webp" srcset="<?= File::image("ingenieur.webp") ?>">
                    <source type="image/png" srcset="<?= File::image("ingenieur.png") ?>">
                    <img width="120" height="120" src="<?= File::image("ingenieur.png") ?>" alt="icon ENSSAT">
                </picture>
            </figure>
            
            <div>
                <h2>SINCE 2020</br>Engineer ITI in apprentice (at ENSSAT)</h2>
                <p>Software and DevOps engineer in apprentice at Ericsson and in ITI training (about computer science) at ENSSAT.</p>
            </div>
        </li>

        <li>
            <figure>
                <picture>
                    <source type="image/webp" srcset="<?= File::image("dut.webp") ?>">
                    <source type="image/png" srcset="<?= File::image("dut.png") ?>">
                    <img width="120" height="120" src="<?= File::image("dut.png") ?>" alt="icon DUT MMI">
                </picture>
            </figure>
            
            <div>
                <h2>2018 - 2020</br>DUT MMI (Métier du Multimédia et de l'Internet)</h2>
                <p>Web developer in apprenticeship at <a href="http://archimaine.fr" target="_blank" rel="noopener noreferrer">Archimaine</a> and in MMI training at Vichy (1st year) and Laval (2nd year).</p>
                <p>Training in communication, audiovisual, graphic/design and web professions</p>
            </div>
        </li>

        <li>
            <figure>
                <picture>
                    <source type="image/webp" srcset="<?= File::image("bac.webp") ?>">
                    <source type="image/png" srcset="<?= File::image("bac.png") ?>">
                    <img width="120" height="120" src="<?= File::image("bac.png") ?>" alt="icon Scientific Baccalaureate">
                </picture>
            </figure>
            
            <div>
                <h2>2015 - 2018</br>Scientific Baccalaureate</h2>
                <p>Scientific Baccalaureate, Life and Earth Sciences option (SVT), ISN specificity (development and robotics).</p>
                <p>Equivalent to a High School Leaving Certificate.</p>
            </div>
        </li>
    </ul>
</section>
<?php $content = ob_get_clean(); ?>


<?php
require(File::page('layout'));
?>
