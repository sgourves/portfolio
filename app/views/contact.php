<?php
use \Models\File;

$title = 'Contact me to discuss !';
$description = 'It\'s the good place to chat over a coffee, to discuss a new project or just out of curiosity!';
?>


<?php ob_start(); ?>
<section id="contact">
    <?php require_once File::parts('contact-form'); ?>
</section>
<?php $content = ob_get_clean(); ?>


<?php
$js = ["mail"];
require(File::page('layout'));
?>
