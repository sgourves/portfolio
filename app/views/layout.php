<?php

use \Models\File;
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<title><?= $title ?> | Steven Gourves</title>

	<meta name="robots" content="follow, index">
	<meta name="author" content="Steven Gourves" />
	<meta name="description" content="<?= $description ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="icon" type="image/png" href="<?= File::image("favicon.svg") ?>" />

	<link rel="preload" href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat&display=swap">
    </noscript>

	<link rel="stylesheet" href="<?= File::css("reset") ?>">
	<link rel="stylesheet" href="<?= File::css("main") ?>">
</head>

<body class="<?= $body ?? "" ?>">

	<header>
		<div class="header-top">
			<a href="/#home" id="logo">
				<?php require_once File::svg("logo"); ?>
			</a>
			<div class="burger" id="mainBurger">
				<div></div>
				<div></div>
				<div></div>
			</div>
		</div>
		<nav id="header-nav">
			<ul>
				<li>
					<a class="header-nav-a" href="/#home">
						<p>Home</p>
						<?php require_once File::svg("home"); ?>
					</a>
				</li>
				<li>
					<a class="header-nav-a" href="/#about-me">
						<p>About me</p>
						<?php require_once File::svg("about"); ?>
					</a>
				</li>
				<li>
					<a class="header-nav-a" href="/#projects">
						<p>Projects</p>
						<?php require_once File::svg("projects"); ?>
					</a>
				</li>
				<li>
					<a class="header-nav-a" href="/contact-me">
						<p>Contact me</p>
						<?php require_once File::svg("contact"); ?>
					</a>
				</li>
				<li>
					<a class="header-nav-a" href="https://www.papierpain.fr" target="_blank" rel="noopener noreferrer">
						<p>The Lab</p>
						<?php require_once File::svg("lab"); ?>
					</a>
				</li>
			</ul>
		</nav>
	</header>

	<main <?= $mainAttr ?? "" ?>>
		<?= $content ?>

		<script type="text/javascript" src="<?= File::js("main") ?>"></script>
		<?php if (isset($js)) { foreach ($js as $value) { ?>
			<script type="text/javascript" src="<?= File::js($value) ?>"></script>
		<?php } } ?>
	</main>

	<footer>
		<?= $footer ?? "" ?>
		<?php if (!isset($footer)) { ?>
			<p>© Copyright 2021 Steven Gourves</p>
			<a href="/my-boring-legal-notices">Legal notices</a>
		<?php } ?>
	</footer>
</body>

</html>
