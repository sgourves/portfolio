<?php
use \Models\File;

$title = 'Portfolio of an engineer in computer science';
$description = 'I\'m Steven Gourves, a passionate software engineer. Multi-field achievements: Development (Java, C, JS, PHP...), Networking, Graphic design, Website...';
?>


<?php ob_start(); ?>
<!-- --------------------- -->
<!-- HOME -->
<section id="home">
    <div class="content">
        <figure>
            <picture>
                <source type="image/webp" srcset="<?= File::image("profile.webp") ?>">
                <source type="image/png" srcset="<?= File::image("profile.png") ?>">
                <img width="190" height="315" src="<?= File::image("profile.png") ?>" alt="profile">
            </picture>
        </figure>
        
        <div>
            <h1>Steven Gourves</h1>
            <p>ITI engineer apprentice at ENSSAT</br>(Computer Science and Information Technology)</p>
        </div>
    </div>

    <a href="/#about-me">
        <img width="64" height="32" src="<?= File::image("arrow_down.png") ?>" alt="arrow down">
    </a>
</section>

<!-- --------------------- -->
<!-- ABOUT ME -->
<section id="about-me">
    <div>
        <p>Hi! I'm an ITI engineer apprentice at ENSSAT, I study in software research and development, multimedia technologies, networks and service design. At the same time, I work as a software and DevOps engineer at Ericsson France (in Lannion).</br><span>I am currently looking for a 3-month internship abroad!</span></p>

        <a href="/my-amazing-career" class="link">
            <p>Career</p>
            <?php require File::svg("arrow_right"); ?>
        </a>
    </div>

    <figure>
        <picture>
            <source type="image/webp" srcset="<?= File::image("skills.webp") ?>">
            <source type="image/png" srcset="<?= File::image("skills.png") ?>">
            <img width="450" height="324" src="<?= File::image("skills.png") ?>" alt="my own skills">
        </picture>
        <figcaption>
            <a href="/public/sgourves-resume.pdf" class="link">
                <p>My resume ❯</p>
            </a>
        </figcaption>
    </figure>
</section>

<!-- --------------------- -->
<!-- PROJECTS -->
<section id="projects" class="sect-list">
    <ul>
        <li>
            <a href="/sspo-visual-design-2020">
                <figure>
                    <picture>
                        <source type="image/webp" srcset="<?= File::image("projects/sspo_main.webp") ?>">
                        <source type="image/jpeg" srcset="<?= File::image("projects/sspo_main.jpg") ?>">
                        <img width="220" height="220" src="<?= File::image("projects/sspo_main.jpg") ?>" alt="Illustration SSPO">
                    </picture>
                </figure>
                <div>
                    <h2>SSPO</br>Identity & Website</h2>
                    <p>Creation of the visual identity of an association (website, logo, graphic charter...)</p>
                    <p class="more">discover >></p>
                </div>
            </a>
        </li>

        <img width="7" height="56" src="<?= File::image("zigouigoui.png") ?>" alt="zigouigoui">

        <li>
            <a href="/newscript-2021">
                <figure>
                    <picture>
                        <source type="image/webp" srcset="<?= File::image("projects/newscript_main.webp") ?>">
                        <source type="image/jpeg" srcset="<?= File::image("projects/newscript_main.jpg") ?>">
                        <img width="220" height="220" src="<?= File::image("projects/newscript_main.jpg") ?>" alt="Illustration NewScript">
                    </picture>
                </figure>
                <div>
                    <h2>NewScript</br>Website & BDD</h2>
                    <p>Application for managing a news competition. It's an academic project.</p>
                    <p class="more">discover >></p>
                </div>
            </a>
        </li>

        <img width="7" height="56" src="<?= File::image("zigouigoui.png") ?>" alt="zigouigoui">

        <li>
            <a href="/papierpain-home-made-servers-2021">
                <figure>
                    <picture>
                        <source type="image/webp" srcset="<?= File::image("projects/papierpain_main.webp") ?>">
                        <source type="image/jpeg" srcset="<?= File::image("projects/papierpain_main.jpg") ?>">
                        <img width="220" height="220" src="<?= File::image("projects/papierpain_main.jpg") ?>" alt="Illustration Home Made Serveur">
                    </picture>
                </figure>
                <div>
                    <h2>PapierPain</br>Home Made Servers</h2>
                    <p>Implementation of continuous integration and network servers (git, file server...)</p>
                    <p class="more">discover >></p>
                </div>
            </a>
        </li>
    </ul>
</section>

<!-- --------------------- -->
<!-- CONTACT -->
<section id="contact">
    <?php require_once File::parts('contact-form'); ?>
</section>
<?php $content = ob_get_clean(); ?>


<?php
$js = ["mail"];
require(File::page('layout'));
?>
