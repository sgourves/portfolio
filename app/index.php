<?php

/** *************************
 * AFFICHAGE DES ERREURS
 */
// ini_set('display_errors',1);

/** *************************
 * ENVIRONNEMENT
 */
require 'env.php';

use \Models\File;
use \Models\Mail;
use \Models\Router\Router;


/** *************************
 * ROUTES
 */
$router = new Router($_SERVER['REQUEST_URI']);

// *** HOME *** //
$router->get('', function() {
	require_once File::page('home');
});

// *** CONTACT *** //
$router->get('/contact-me', function() {
	require_once File::page('contact');
});
$router->post('/contact', function() {
	Mail::send(
		$_POST['firstname'],
		$_POST['lastname'],
		$_POST['email'],
		$_POST['subject'],
		$_POST['message'],
		($_POST['checkContact'] == "on" && $_POST['contact'] == "")
	);
});

// *** CAREER *** //
$router->get('/my-amazing-career', function() {
	require_once File::page('career');
});

// *** PROJECTS *** //
// newscript
$router->get('/newscript-2021', function() {
	require_once File::page('projects/newscript-2021');
});
// sspo
$router->get('/sspo-visual-design-2020', function() {
	require_once File::page('projects/sspo-2020');
});
// startup week end
$router->get('/startup-week-end-2019', function() {
	require_once File::page('projects/startup-week-end-2019');
});
// home made servers
$router->get('/papierpain-home-made-servers-2021', function() {
	require_once File::page('projects/papierpain-2021');
});

// *** MENTIONS-LEGALES *** //
$router->get('/my-boring-legal-notices', function() {
	require_once File::page('legalnotices');
});

$router->run();
